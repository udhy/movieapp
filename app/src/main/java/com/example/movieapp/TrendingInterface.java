package com.example.movieapp;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TrendingInterface {
    @GET("/3/{category}")
    Call<TrendingMovieResults>listOfMovies(
      @Path("category") String category,
      @Query("api_key") String api_key
    );
}


/*
https://api.themoviedb.org/3/movie/popular?api_key=a395f729b81915c8ec55ce87929b8060&language=en-US&page=1
 */