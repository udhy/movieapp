package com.example.movieapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieActivity extends AppCompatActivity {
    public static String BASE_URL = "https://api.themoviedb.org";
    public static String API_KEY = "a395f729b81915c8ec55ce87929b8060";
    public static String LANGUAGE = "en-US";

    private static TextView title;
    private static TextView overview;
    private static TextView rating;
    private static Integer id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        Intent intent = getIntent();
        id = intent.getExtras().getInt("id");

        title = (TextView) findViewById(R.id.title);
        overview = (TextView) findViewById(R.id.overview);
        rating = (TextView)findViewById(R.id.rating);
        Context context = this;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MovieInterface myInterface = retrofit.create(MovieInterface.class);
//        String category = CATEGORY + "/" + String.valueOf(ID);
        Call<MovieResults> call = myInterface.MovieInfo(id, API_KEY, LANGUAGE);
        call.enqueue(new Callback<MovieResults>() {
            @Override
            public void onResponse(Call<MovieResults> call, Response<MovieResults> response) {
                MovieResults results = response.body();
                System.out.println(response.errorBody());
                System.out.println(response.message());
                System.out.println(call.toString());
                System.out.println(response.toString());
                System.out.println();
                if(results != null) {
                    title.setText(results.getTitle());
                    overview.setText(results.getOverview());
                    rating.setText(String.valueOf(results.getVote_average()));
                }
                // List<MovieResults.BelongsToCollectionDTO> listOfMovies = results.getGenres();
            }

            @Override
            public void onFailure(Call<MovieResults> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }
}

