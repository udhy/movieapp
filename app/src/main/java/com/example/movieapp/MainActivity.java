package com.example.movieapp;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private static Button trending_button;
    private static Button playing_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        trending_button = (Button) findViewById(R.id.trending_button);
        playing_button = (Button) findViewById(R.id.playing_button);
        trending_button.setOnClickListener(this);
        playing_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.playing_button)
        {
            Intent myIntent = new Intent(this, PlayingActivity.class);
            this.startActivity(myIntent);
        }
        if(v.getId() == R.id.trending_button)
        {
            Intent myIntent = new Intent(this, TrendingActivity.class);
            this.startActivity(myIntent);
        }
    }
}

