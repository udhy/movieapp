package com.example.movieapp;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PlayingInterface {
    @GET("/3/movie/{category}")
    Call<PlayingMovieResults>listOfMovies(
            @Path("category") String category,
            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") Integer page
    );
}


/*
https://api.themoviedb.org/3/movie/popular?api_key=a395f729b81915c8ec55ce87929b8060&language=en-US&page=1
 */