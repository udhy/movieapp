package com.example.movieapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TrendingActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{
    public static String BASE_URL = "https://api.themoviedb.org";
    public static String CATEGORY = "trending/movie/week";
    public static String API_KEY = "a395f729b81915c8ec55ce87929b8060";

    private static ListView myListView;
    private static List<Integer> idList = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trending);

        myListView = (ListView) findViewById(R.id.trending_list);
        Context context = this;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        TrendingInterface myInterface = retrofit.create(TrendingInterface.class);
        Call<TrendingMovieResults> call = myInterface.listOfMovies(CATEGORY, API_KEY);
        call.enqueue(new Callback<TrendingMovieResults>() {
            @Override
            public void onResponse(Call<TrendingMovieResults> call, Response<TrendingMovieResults> response) {
                TrendingMovieResults results = response.body();
                List<TrendingMovieResults.ResultsDTO> listOfMovies = results.getResults();
                List<String> movieList = new ArrayList<String>();
                for (int i = 0; i < listOfMovies.size(); i++) {
                    TrendingMovieResults.ResultsDTO movie = listOfMovies.get(i);
                    movieList.add(movie.getTitle());
                    idList.add(movie.getId());
                }
                System.out.println(movieList.size());
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, R.layout.activity_listview, R.id.textView, movieList);
                myListView.setAdapter(arrayAdapter);
                myListView.setOnItemClickListener(TrendingActivity.this);
            }

            @Override
            public void onFailure(Call<TrendingMovieResults> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view,
                            int position, long id) {
        Intent myIntent = new Intent(this, MovieActivity.class);
        myIntent.putExtra("id", idList.get(position));
        this.startActivity(myIntent);
    }
}
