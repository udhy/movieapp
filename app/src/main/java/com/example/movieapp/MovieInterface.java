package com.example.movieapp;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieInterface {
    @GET("/3/movie/{id}")
    Call<MovieResults> MovieInfo(
            @Path("id") Integer id,
            @Query("api_key") String api_key,
            @Query("language") String language
    );
}
